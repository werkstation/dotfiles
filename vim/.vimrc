"
" ~/.vimrc 19/04/2009 18:17

"set ttymouse=xterm2
set mouse=
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'dracula/vim'
Plugin 'itchyny/lightline.vim'
Plugin 'ekalinin/Dockerfile.vim'
Plugin 'avakhov/vim-yaml'
Plugin 'itchyny/vim-gitbranch'
Plugin 'tarekbecker/vim-yaml-formatter'
Plugin 'christianrondeau/vim-base64'

let g:lightline = {
	  \ 'colorscheme': 'srcery_drk',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'gitbranch#name'
      \ },
      \ }

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

set history=1
set autoread

" Search
set hlsearch
set incsearch

" Key behavior
set backspace=indent,eol,start
set nocompatible

" Indent settings.
set tabstop=2       " number of visual spaces per TAB
set softtabstop=4   " number of spaces in tab when editing
set shiftwidth=4    " number of spaces to use for autoindent
set expandtab       " tabs are space
set autoindent
set copyindent

" Appareance settings
set ruler
set laststatus=2
set showmatch
set showcmd
set lazyredraw
set shortmess=at
set statusline=%F%m%r%h%w\ (%Y)\ (%l,%v)[%p%%]
set nostartofline
set cursorline

" Folds 
set foldmethod=marker
set foldenable

" Filters
set wildmenu
set wildignore=*.o,*#,*~,*.dll,*.so,*.a

set fillchars=""
set number
syntax on
colorscheme zellner
hi StatusLine term=NONE cterm=NONE ctermfg=1 ctermbg=NONE
hi EndOfBuffer ctermfg=black guifg=black
hi CursorLine cterm=NONE ctermbg=238 guibg=darkred guifg=white
