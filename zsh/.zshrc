ZSH_THEME="muse"

plugins=(git kubectl history-substring-search)

for c (~/.zsh/*.zsh) source $c
source $ZSH/oh-my-zsh.sh
