kbash() {
  COLUMNS=`tput cols`
  LINES=`tput lines`
  TERM=xterm
  kubectl exec -i -t $1 env COLUMNS=$COLUMNS LINES=$LINES TERM=$TERM bash
}

kgpn() {
  if [ -n "$1" ] ; then
    kubectl get pods | grep $1 | cut -d\  -f1
  else
    kubectl get pods -o wide
  fi
}

kgpg() {
	kgpa | grep $1
}

kw() {
	watch -n1 "kubectl get pods -owide | grep $1"
}

kdelpn() {
	for k in $(kgpn $1) ; do
		kdelp $k
	done
}

krun() {
  for k in $(kubectl get nodes -o jsonpath='{.items[*].metadata.name}' ) ; do
    echo $k | grep -v closed
    ssh -qt $k $1
  done
}

krash() {
	kubectl get pods -owide --all-namespaces | perl -wanl -e '( $. == 1
    || ( eval($F[2]) != 1 && $F[3] !~ /Completed/ )
   ) && print'
}

kerror() {
  for c in $(kgpa | grep -i error | awk '{print $2}'); do 
    kdelp $c
  done
}

ssh() {
	autossh "$@"
}

enc() {
	echo -n "$1" | base64
}

dec() {
	echo -n "$1" | base64 -D
}

t() {
  terraform $@
}

xcp() {
  xsel -i -b
}

xpa() {
  xsel -o -b
}

pgen() {
  </dev/urandom tr -cd '!-~' | fold -w ${1:-16} | sed 1q
}
