export AUTOSSH_PORT=0
export EDITOR=nvim
export PATH=/usr/local/sbin:$HOME/b:$PATH
export SAFE_DIR=~/.secrets
export TERM="xterm-256color"
export VISUAL=nvim
export ZSH=~/.oh-my-zsh
