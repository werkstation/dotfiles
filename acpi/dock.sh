#!/bin/sh

export DISPLAY=:0

xrandr -d :0.0 --output LVDS1 --off --output DP1 --off --output DP2 --off --output DP3 --off --output HDMI1 --off --output HDMI2 --mode 1920x1080 --pos 0x0 --rotate normal --output HDMI3 --mode 1920x1080 --pos 1920x0 --rotate normal --output VGA1 --off --output VIRTUAL1 --off && feh --bg-fill /home/vmaillot/p/tpb.jpg
