#!/bin/bash

tmpfile="p.tmp"
ffile="pwds.gpg"

echo "reading nuglif password..."
read -s p
echo "set my_nuglif_pass = '$p'" > $tmpfile
echo "reading perso password..."
read -s p
echo "set my_perso_pass = '$p'" >> $tmpfile
echo "reading pro password..."
read -s p
echo "set my_pro_pass = '$p'" >> $tmpfile
gpg --recipient vmaillot@nuglif.com -e $tmpfile
mv $ffile ${ffile}.$(date "+%d%m%Y")
mv $tmpfile.gpg $ffile
rm $tmpfile
